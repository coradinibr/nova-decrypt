import sbt.Keys._

lazy val commonSettings = Seq(
   organization := "net.coradini",
   version := "0.1.0",
   scalaVersion := "2.11.2",
   resolvers ++= Seq(
      "Typesafe repository releases" at "http://repo.typesafe.com/typesafe/releases/",
      "Typesafe Repository 2" at "http://repo.typesafe.com/typesafe/maven-releases/",
      "Sonatype Releases" at "http://oss.sonatype.org/content/repositories/releases/",
      "Maven Central" at "https://repo1.maven.org/maven2",
      "softprops-maven" at "http://dl.bintray.com/content/softprops/maven",
      "Job Server Bintray" at "https://dl.bintray.com/spark-jobserver/maven"
   )
)

lazy val root = (project in file(".")).
   settings(commonSettings: _*).
   settings(
      name := "sc",
      libraryDependencies ++= Seq(
         "com.twitter" %% "util-logging" % "6.37.0",
         "com.lambdaworks" % "scrypt" % "1.4.0"
      )
   )