package net.coradini

import java.nio.charset.StandardCharsets
import java.util.Base64
import javax.crypto.Cipher
import javax.crypto.spec.{IvParameterSpec, SecretKeySpec}

/**
  * Created by coradinibr on 22/10/16.
  */

object Main {

   def main(args: Array[String]): Unit = {

      val map: Map[Long, String] = Map(
         (26343307L -> "ExxkAz6mFC8cCyMtTVD5cA=="),
      (27512821L -> "tkpgGwPxDLwXEmGEpUYoLw=="),
      (26767553L -> "C2fQp5ZPfA+HucPejMo2vg=="),
      (26232733L -> "gSM+Q/SQelybpxTTgmaFuA=="),
      (24988778L -> "SUJcCXkIKU6bALp3368iRA=="),
      (22736291L -> "qFhKu4mPPUcAil1RzYd5gg=="),
      (28771650L -> "U6LTLC4hZncRjbw+goqFjA=="),
      (28747548L -> "KX/NtkUD6ihkSqVDLe03Uw=="),
      (28562447L -> "Yrssfm1SUzIiJOmc9L3QcA=="),
      (28526098L -> "WfrUsg2JY7PRVrKA1wBJxg=="),
      (26717642L -> "0SJPi76mp2/B5FJObEWgMw=="),
      (21951272L -> "DqP7T6Xke5DJAcey1bbDRw=="),
      (28124081L -> "VQpO2OdlZUvFtHvxlHleVg=="),
      (26059625L -> "0InU0SLbDeaDcbQsVz/VcQ=="),
      (28195836L -> "PJ/xD8lBus0lFPnNPVa+oQ=="),
      (23329770L -> "BDQfqyA+kYJYtMURBDsBPA=="),
      (27841626L -> "pg9xwJsdVvBEO6430wqz6A=="),
      (26992808L -> "xJ1CxewJOlAT9OEp0CQW/g=="),
      (26769375L -> "igWFD70Ku3IyohikwbClnw=="),
      (25262355L -> "r0uzR+/2N/Vh6YHRb45ztg=="),
      (20544726L -> "3SqvEKJTpcIo9La7nsO3UQ=="),
      (26571151L -> "A5b/71z5RfIje2hHQwhZzA==")
      )

      //val input = "3ELOmkXJTj4fgYrXEkX/Fg=="

      map.foreach( p => {
         var passwd = EncryptionUtil.decrypt(p._2)
         println(s"${p._1}\tclear: ${passwd}\t\t\thash:${Crypto.hash(passwd)}")
      })
   }
}

object EncryptionUtil {

   private def hex2bytes(hex: String): Array[Byte] = {
      // Only printable chars
      hex.replaceAll("[^0-9A-Fa-f]", "").sliding(2, 2).toArray.map(Integer.parseInt(_, 16).toByte)
   }

   private lazy val key = hex2bytes("F5B4BD3F19F42BDC38EDF5AE91648F13AEF169425C56A8BF930163B6CF53D0BF")
   private lazy val iv = hex2bytes("F0DF8B290C6638640BA209CFE0369C79")
   private val cipherAlgorithms = "AES/CBC/PKCS5Padding"
   private val minKeySize = 256

   def decrypt(passwordBase64: String) : String = {
      // check if AES-256 is available
      if (Cipher.getMaxAllowedKeyLength(cipherAlgorithms) < minKeySize) {
         throw new IllegalStateException("Unlimited crypto files not present in this JRE. Please install JCE.")
      }

      try {
         val cipher = Cipher.getInstance(cipherAlgorithms)
         cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key, "AES"), new IvParameterSpec(iv))
         val encryptedData = Base64.getDecoder.decode(passwordBase64)
         val decryptedData = cipher.doFinal(encryptedData)
         new String(decryptedData, StandardCharsets.UTF_8)
      }
      catch {
         case t: Throwable =>
            ""
      }
   }
}