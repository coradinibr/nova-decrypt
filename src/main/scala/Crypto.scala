package net.coradini

import com.lambdaworks.crypto.SCryptUtil

/**
  * Created by ecoradini on 11/13/16.
  */
object Crypto {

   var N = 32
   val r = 8
   val p = 1

   def hash(password: String) = {
      SCryptUtil.scrypt(password, N, r, p)
   }

   def check(unhashed: String, hashed: String) = {
      SCryptUtil.check(unhashed, hashed)
   }

   def isHashed(maybeHashed: String) = {
      val parts = maybeHashed.split("\\$")
      if (parts.length != 5 || !parts(1).equals("s0")) false else true
   }
}
